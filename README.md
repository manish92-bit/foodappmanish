Spring boot application for upload data in database with RESTful service for Food Delivery app.


## Tools and Technologies used

* Spring boot 2.1.2
* MongoDB
* Spring Tool Suite
* Lombok
* Java1.8

mvn spring-boot:run## Step to install

The backend server will start at <http://localhost:8080>.