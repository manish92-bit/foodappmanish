package in.manish.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import in.manish.restapi.dao.MerchantDetailsDAO;
import in.manish.restapi.model.MerchantDetails;
import in.manish.restapi.service.SequenceGeneratorService;

@RestController
@RequestMapping("/api")
public class MerchantDetailsController {

	@Autowired
	MerchantDetailsDAO merchantDetailsDAO;
	
	@Autowired
	SequenceGeneratorService seqGeneratorService;
	
	@PostMapping("/create")
	public MerchantDetails create(@RequestBody MerchantDetails newEmployeeObject) {
		newEmployeeObject.setId(seqGeneratorService.generateSequence(MerchantDetails.SEQUENCE_NAME));
		return merchantDetailsDAO.save(newEmployeeObject);
	}
}

