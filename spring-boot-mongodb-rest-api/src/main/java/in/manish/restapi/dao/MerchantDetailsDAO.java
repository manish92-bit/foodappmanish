package in.manish.restapi.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import in.manish.restapi.model.MerchantDetails;

@Repository
public interface MerchantDetailsDAO extends MongoRepository<MerchantDetails, Long> {

}
