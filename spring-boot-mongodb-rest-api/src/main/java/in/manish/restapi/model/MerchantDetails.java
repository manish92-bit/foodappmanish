package in.manish.restapi.model;

import java.math.BigDecimal;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor


@Document(collection = "merchantDetails")
public class MerchantDetails {
	
	public static final String SEQUENCE_NAME = "employees_sequence"; 
	
	@Id
	private Long id;
	
	private String restaurentName ;
	
	private String contactName;
	
	private Integer pin;
	
	private String location;
	
	private String website;
	
	private String phoneNumber;
	
	private BigDecimal averageDailyTransaction;

	
	
}
